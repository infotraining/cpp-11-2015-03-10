#include <iostream>

using namespace std;

void works_with_pointer(nullptr_t ptr)
{
	cout << "special nullptr impl: " << static_cast<void*>(ptr) << endl;
}

void works_with_pointer(int* ptr)
{
	if (ptr != nullptr)
		cout << "value: " << *ptr << endl;
	else
		cout << "nullptr" << endl;
}

void works_with_pointer(int value)
{
	cout << "int value: " << value << endl;
}

int main()
{
	int x = 10;

	works_with_pointer(&x);

	int* ptr = 0;

	works_with_pointer(ptr);

	works_with_pointer(nullptr);

	int* p1 {};
	int* p2 = nullptr;

	system("PAUSE");
}