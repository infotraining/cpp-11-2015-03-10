#include <iostream>
#include <vector>
#include <algorithm>
#include <memory>
#include <string>

using namespace std;

template <typename T, typename... Args>
unique_ptr<T> my_make_unique(Args&&... args)
{
	return unique_ptr<T>(new T(forward<Args>(args)...));
}


class Array
{
	int* data_;
	size_t size_;
public:
	using iterator = int*;
	using const_iterator = const int*;

	Array(size_t size = 10) : data_(new int[size]), size_(size)
	{
		fill(data_, data_ + size_, 0);
		cout << "Ctor of array" << endl;
	}

	Array(initializer_list<int> lst) : Array(lst.size())
	{
		copy(lst.begin(), lst.end(), data_);

		cout << "Ctor of array with il: ";

		for (const auto& item : lst)
			cout << item << " ";
		cout << endl;
	}

	Array(const Array& source) : Array(source.size())
	{
		copy(source.data_, source.data_ + size_, data_);
		cout << "Copy ctor of array: " << data_ << endl;
	}

	Array& operator=(const Array& source)
	{
		if (this != &source)
		{
			int* temp_data = new int[source.size()];
			copy(source.begin(), source.end(), temp_data);

			delete[] data_;

			data_ = temp_data;
			size_ = source.size_;

			cout << "Copy operator= of array: " << data_ << endl;
		}

		return *this;
	}

	Array(Array&& source) : data_(source.data_), size_(source.size_)
	{
		source.data_ = nullptr;
		source.size_ = 0;

		cout << "Move ctor of array: " << data_ << endl;
	}

	Array& operator=(Array&& source)
	{
		if (this != &source)
		{
			delete[] data_;
			data_ = source.data_;
			size_ = source.size_;
			source.data_ = nullptr;
			source.size_ = 0;

			cout << "Move operator= of array: " << data_ << endl;
		}

		return *this;
	}

	~Array()
	{
		delete[] data_;
	}

	int& operator[](size_t index)
	{
		return data_[index];
	}

	const int& operator[](size_t index) const
	{
		return data_[index];
	}

	size_t size() const
	{
		return size_;
	}

	iterator begin()
	{
		return data_;
	}

	iterator end()
	{
		return data_ + size_;
	}

	const_iterator begin() const
	{
		return data_;
	}

	const_iterator end() const
	{
		return data_ + size_;
	}
};

void print_all(const Array& arr)
{
	for (const auto& item : arr)
		cout << item << " ";
	cout << endl;
}

Array create_array(size_t size)
{
	auto arr = Array(size);

	for (auto& item : arr)
		item = -1;

	return arr;
}

class Owner
{
	string name;
	Array array;
public:
	Owner(const string& name, size_t size) : name(name), array(size)
	{}

	Owner(Owner&& source) : name(move(source.name)), array(move(source.array))
	{}

	Owner& operator=(Owner&& source)
	{
		if (this != &source)
		{
			name = move(source.name);
			array = move(source.array);
		}

		return *this;
	}

	void print() const
	{
		cout << "Owner: " << name << " ";
		print_all(array);
	}

	//~Owner() { cout << "~Owner" << endl; }
};

int main()
{
	Array arr1(10);

	print_all(arr1);

	Array arr2 = { 1, 2, 3, 4, 5, 6 };

	print_all(arr2);

	Array arr3 = arr2;

	cout << "\n\n";

	Array arr4 = move(arr3);

	cout << "\n--vector:\n";

	vector<Array> vec;

	vec.push_back(Array({ 1, 2, 3 }));
	vec.push_back(move(arr4));
	vec.push_back({ 1, 2, 3, 4, 5 });
	vec.emplace_back(12);

	print_all(vec[0]);

	auto arr5 = create_array(5);

	print_all(arr5);

	cout << "\n\n";

	const Array carr = { 1, 2, 3, 4, 5 };

	Array arr6 = move(carr);

	cout << "\n\n";

	Owner o1{ "Kowalski", 10 };

	Owner o2 = move(o1);

	vector<Owner> owners;

	owners.push_back(move(o2));
	owners.push_back(Owner("Nowak", 10));
	owners.emplace_back("Nowak", 10);

	auto&& temp_arr = create_array(10);

	Array dest_arr = move(temp_arr);

	vector<bool> vec_bool = { 1, 0, 0, 0, 1, 1, 1, 1 };

	for (auto&& item : vec_bool)
	{
		cout << item << " ";
	}
	cout << endl;

	for (auto&& item : vec_bool)
	{
		item = 0;
	}

	for (auto&& item : vec_bool)
	{
		cout << item << " ";
	}
	cout << endl;

	{
		auto ptr = make_unique<Owner>("Kowalski", 10);

		ptr->print();
	}

	system("PAUSE");
}