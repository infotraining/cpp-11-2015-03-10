#include <iostream>

class BankAccount
{
    const int id_;
    double balance_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_ << "; Balance = " << balance_ << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        balance_ -= amount;
        to.balance_ += amount;
    }

    void withdraw(double amount)
    {
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        return balance_;
    }
};


int main()
{
    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();
}
