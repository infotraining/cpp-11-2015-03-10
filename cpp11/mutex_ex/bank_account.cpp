#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

class BankAccount
{
    const int id_;
    double balance_;
	mutable mutex mtx_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
		std::cout << "Bank Account #" << id_ << "; Balance = ";
		unique_lock<mutex> lk(mtx_);
		cout << balance_;
		lk.unlock();
		cout << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
		/*lock_guard<mutex> lk_from(mtx_);
		lock_guard<mutex> lk_to(to.mtx_);*/
		unique_lock<mutex> lk_from(mtx_, defer_lock);
		unique_lock<mutex> lk_to(to.mtx_, defer_lock);
		lock(lk_from, lk_to);
		balance_ -= amount;
		to.balance_ += amount;
    }

    void withdraw(double amount)
    {
		lock_guard<mutex> lk(mtx_);

        balance_ -= amount;
    }

    void deposit(double amount)
    {
		lock_guard<mutex> lk(mtx_);
        
		balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
		lock_guard<mutex> lk(mtx_);

        return balance_;
    }
};

void make_withdraws(BankAccount& ba, size_t no_of_operations, double amount)
{
	for (size_t i = 0; i < no_of_operations; ++i)
		ba.withdraw(amount);
}

void make_deposits(BankAccount& ba, size_t no_of_operations, double amount)
{
	for (size_t i = 0; i < no_of_operations; ++i)
		ba.deposit(amount);
}

void make_transfers(BankAccount& from, BankAccount& to, size_t no_of_operations, double amount)
{
	for (size_t i = 0; i < no_of_operations; ++i)
		from.transfer(to, amount);
}

int main()
{
    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

	cout << "\n\n";

	/*thread thd1{ &make_withdraws, ref(ba1), 10000, 1.0 };
	thread thd2{ [&ba1] { make_deposits(ba1, 10000, 1.0); } };*/

	thread thd1{ [&] { make_transfers(ba1, ba2, 10000, 1.0); } };
	thread thd2{ [&] { make_transfers(ba2, ba1, 10000, 1.0); } };

	thd1.join();
	thd2.join();

	ba1.print();

	ba2.print();

	system("PAUSE");
}
