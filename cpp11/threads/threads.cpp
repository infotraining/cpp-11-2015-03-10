#include <iostream>
#include <thread>
#include <chrono>
#include <memory>
#include <vector>


using namespace std;

void hello(const string& text = "Hello concurrent world!", size_t delay = 300)
{
	for (const auto& c : text)
	{
		cout << c << " ";
		this_thread::sleep_for(chrono::milliseconds(delay));
		cout.flush();
	}

	cout << "End of hello..." << endl;
}

void save_to_file(const string& file_name)
{
	for (const auto& c : file_name)
	{
		cout << c << " ";
		this_thread::sleep_for(chrono::milliseconds(4000));
		cout.flush();
	}
}

void may_throw()
{
	if (rand() % 13)
		throw runtime_error("Error#13");
}

class ThreadJoinGuard
{
	thread& thd_;
public:
	ThreadJoinGuard(thread& thd) : thd_(thd) {}

	ThreadJoinGuard(const ThreadJoinGuard&) = delete;
	ThreadJoinGuard& operator=(const ThreadJoinGuard&) = delete;

	~ThreadJoinGuard()
	{
		if (thd_.joinable())
			thd_.join();
	}
};

class ThreadDetachGuard
{
	thread& thd_;
public:
	ThreadDetachGuard(thread& thd) : thd_(thd) {}

	ThreadDetachGuard(const ThreadDetachGuard&) = delete;
	ThreadDetachGuard& operator=(const ThreadDetachGuard&) = delete;

	~ThreadDetachGuard()
	{
		thd_.detach();
	}
};

//class ThreadGuard
//{
//public:
//	enum class DtorAction { join, detach };
//
//	ThreadGuard(DtorAction action, thread&& thd) : thd_{ move(thd) }, action_{ action }
//	{}
//
//	template <typename... T>
//	ThreadGuard(DtorAction action, T&&... args) : action_{ action }, thd_{ forward<T>(args)... }
//	{}
//
//	ThreadGuard(const ThreadGuard&) = delete;
//	ThreadGuard& operator=(const ThreadGuard&) = delete;
//
//	ThreadGuard(ThreadGuard&&) = default;
//	ThreadGuard& operator=(ThreadGuard&&) = default;
//
//	~ThreadGuard()
//	{
//		if (action_ == DtorAction::join)
//		{
//			if (thd_.joinable())
//				thd_.join();
//		}
//		else
//			thd_.detach();
//	}
//
//	thread& get() const
//	{
//		return thd_;
//	}
//
//private:
//	thread thd_;
//	DtorAction action_;
//};

void run_as_deamon()
{
	thread thd([]{save_to_file("drawing.txt"); });

	auto detach_thread = [](thread* thd) { if (thd->joinable()) thd->detach(); };
	unique_ptr<thread, decltype(detach_thread)> at_scope_detach(&thd, detach_thread);

	// alternative version
	//shared_ptr<thread> at_scope_detach(&thd, [](thread* thd) { if (thd->joinable()) thd->detach(); });

	may_throw();

	thd.detach();
}

void run_as_deamonwith_raii()
{
	thread thd([]{save_to_file("drawing.txt"); });

	ThreadDetachGuard guard(thd);

	//ThreadGuard guard1{ ThreadGuard::DtorAction::detach, thread{ &hello, "Tekst", 200 } };
	//ThreadGuard guard2{ ThreadGuard::DtorAction::join, &hello, 1, 300 };

	may_throw();
}

void main() try
{
	//auto deamon_thread = run_as_deamon();
	run_as_deamon();

	thread thd1(&hello, "TEXT", 3000);
	thread thd2([]{ hello("Another thread...", 100); });

	hello();

	thd1.detach();
	thd2.join();

	system("PAUSE");

	//deamon_thread.join();
}
catch (const exception& e)
{
	cout << "Caught exception : " << e.what() << endl;

	system("PAUSE");
}