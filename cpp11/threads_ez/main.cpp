#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <thread>
#include <atomic>
#include <algorithm>
#include <numeric>
#include <mutex>
#include <future>

using namespace std;

//atomic<long> counter(0);

long calc_hits(long N)
{
	random_device rd;
	mt19937_64 gen(rd());
	uniform_real_distribution<> dis(-1, 1);
	long counter = 0;
	for (int n = 0; n < N; ++n)
	{
		double x = dis(gen);
		double y = dis(gen);
		if (x*x + y*y < 1)
			counter++;
	}
	return counter;
}

void sync_pi(long N)
{
	cout << "Pi calc - synchronous" << endl;

	auto start = chrono::high_resolution_clock::now();
	cout << double(calc_hits(N) / double(N)) * 4 << endl;
	auto end = chrono::high_resolution_clock::now();

	cout << "Elapsed = ";
	cout << chrono::duration_cast<chrono::milliseconds>(end - start).count();
	cout << " ms" << endl;
}

void threads_pi(long N)
{
	vector<thread> thds;
	int n_of_threads = thread::hardware_concurrency();

	vector<long> res(n_of_threads);

	auto start = chrono::high_resolution_clock::now();

	for (int i = 0; i < n_of_threads; ++i)
	{
		thds.emplace_back([i, &res, N, n_of_threads]() {
			res[i] = calc_hits(N / n_of_threads);
		});
	}

	for (auto& th : thds) th.join();

	cout << double(accumulate(res.begin(), res.end(), 0L) / double(N)) * 4 << endl;
	//cout << double(counter)/double(N)*4 << endl;
	auto end = chrono::high_resolution_clock::now();
	cout << "Elapsed parallel = ";
	cout << chrono::duration_cast<chrono::milliseconds>(end - start).count();
	cout << " ms" << endl;
}

void shared_pi(long N)
{
	long shared_counter = 0L;
	mutex shared_counter_mtx;
	int n_of_threads = thread::hardware_concurrency();
	long n_of_iterations = N / n_of_threads;

	cout << "\n\nBad version:\n";

	vector<thread> threads;

	auto start = chrono::high_resolution_clock::now();

	for (int i = 0; i < n_of_threads; ++i)
		threads.emplace_back([=, &shared_counter, &shared_counter_mtx]() {
		random_device rd;
		mt19937_64 gen(rd());
		uniform_real_distribution<> dis(-1, 1);
		for (int n = 0; n < n_of_iterations; ++n)
		{
			double x = dis(gen);
			double y = dis(gen);
			if (x*x + y*y < 1)
			{
				lock_guard<mutex> lk(shared_counter_mtx);  // mtx.lock();
				shared_counter++;
			} // mtx.unlock();
		}
	});

	for (auto& thd : threads)
		thd.join();

	auto end = chrono::high_resolution_clock::now();

	cout << double(shared_counter / double(N)) * 4 << endl;

	cout << "Elapsed parallel = ";
	cout << chrono::duration_cast<chrono::milliseconds>(end - start).count();
	cout << " ms" << endl;
}

void async_pi(long N)
{
	vector<future<long>> hits;

	int n_of_threads = thread::hardware_concurrency();

	auto start = chrono::high_resolution_clock::now();

	for (int i = 0; i < n_of_threads; ++i)
		hits.push_back(async(launch::async, [=] { return calc_hits(N / n_of_threads); }));

	long total_hits = accumulate(hits.begin(), hits.end(), 0L, [](long val, future<long>& f) { return val + f.get(); });

	auto end = chrono::high_resolution_clock::now();

	cout << double(total_hits / double(N)) * 4 << endl;

	cout << "Elapsed parallel = ";
	cout << chrono::duration_cast<chrono::milliseconds>(end - start).count();
	cout << " ms" << endl;
}

int main()
{
	long N = 100000000;
	
	sync_pi(N);

	threads_pi(N);

	shared_pi(N);
	
	async_pi(N);	

	system("PAUSE");
}

