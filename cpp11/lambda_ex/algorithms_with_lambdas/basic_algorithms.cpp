#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <random>

using namespace std;

int main()
{
	vector<int> vec(25);

	std::random_device rd;
	std::mt19937 mt{ rd() };
	std::uniform_int_distribution<int> uniform_dist{ 1, 30 };

	generate(vec.begin(), vec.end(), [&] { return uniform_dist(mt); });

	cout << "vec: ";
	for (const auto& item : vec)
		cout << item << " ";
	cout << endl;

	// 1 - utworz wektor parzystych
	vector<int> evens;
	auto is_even = [](int x) { return x % 2 == 0; };
	copy_if(vec.begin(), vec.end(), back_inserter(evens), is_even);

	for (const auto& item : evens)
		cout << item << " ";
	cout << endl;

    // 1a - wyświetl parzyste
	for (const auto& item : vec)
		if (is_even(item))
			cout << item << " ";

	copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "), is_even);
	cout << endl;

    // 1b - wyswietl ile jest parzystych
	cout << "ilosc parzystych: " << count_if(vec.begin(), vec.end(), is_even) << endl;

    int eliminators[] = { 3, 5, 7 };
    // 2 - usuń liczby podzielne przez dowolną liczbę z tablicy eliminators
	vec.erase(remove_if(vec.begin(), vec.end(), 
		[&eliminators](int x) { 
			return any_of(begin(eliminators), end(eliminators), [x](int y) { return x % y == 0; });
	}), vec.end());
	
	for (const auto& item : vec)
		cout << item << " ";
	cout << endl;

    // 3 - tranformacja: podnieś liczby do kwadratu
	transform(vec.begin(), vec.end(), vec.begin(), [](int x) { return x * x; });

	for (const auto& item : vec)
		cout << item << " ";
	cout << endl;

    // 4 - wypisz 5 najwiekszych liczb
	nth_element(vec.begin(), vec.begin() + 5, vec.end(), [](int a, int b) { return a > b; });
	copy(vec.begin(), vec.begin() + 5, ostream_iterator<int>(cout, " "));
	cout << endl;

    // 5 - policz wartosc srednia
	auto avg = accumulate(vec.begin(), vec.end(), 0.0) / vec.size();

	cout << "avg: " << avg << endl;

    // 6 - utwórz dwa kontenery - 1. z liczbami mniejszymi lub równymi średniej, 2. z liczbami większymi od średniej
	
	vector<int> ls_eq_avg, gt_avg;

	partition_copy(vec.begin(), vec.end(), back_inserter(ls_eq_avg), back_inserter(gt_avg), [avg](int x) { return x <= avg; });

	cout << "ls_eq_avg: ";
	for (const auto& item : ls_eq_avg)
		cout << item << " ";
	cout << endl;

	cout << "gt_avg: ";
	for (const auto& item : gt_avg)
		cout << item << " ";
	cout << endl;
}
