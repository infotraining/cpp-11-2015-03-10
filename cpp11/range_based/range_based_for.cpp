#include <iostream>
#include <vector>
#include <set>
#include <string>
#include <map>
#include <functional>

using namespace std;

template <typename Container>
void print_all(const Container& cont)
{
	for (const auto& item : cont)
	{
		cout << item << " ";
	}
	cout << endl;
}

int add1(int a, int b)
{
	return a + b;
}

struct Vector2D
{
	double x, y;
};

double operator*(const Vector2D& v1, const Vector2D& v2)
{
	return 42.0;
}

template <typename T>
auto multiply(const T& a, const T& b) -> decltype(a*b)
{
	return a * b;
}

int main()
{
	vector<int> vec = { 1, 2, 3, 5, 9 };

	for (auto item : vec)
		cout << item << " ";
	cout << endl;

	string words[] = { "one", "two", "three" };

	for (const auto& w : words)
		cout << w << " ";
	cout << endl;

	//for (auto it = begin(words); it != end(words); ++it)
	//{
	//	const auto& w = *it;
	//	cout << w << " ";
	//}

	for (auto item : { 1, 2, 3, 4 })
		cout << item << " ";
	cout << endl;

	print_all(vec);

	auto lst = { 16.9, 45.3, 66.5 };
	print_all(lst);
	
	print_all(vector<int>({ 1, 4, 6, 89 }));
	
	print_all(words);

	map<int, string, greater<int>> dict = { { 1, "one" }, { 2, "two" }, { 3, "three" } };

	//map<int, string, greater<int>>::iterator it = dict.begin();

	auto it = dict.begin();

	while (it != dict.end())
	{
		if (it->first == 2)
			it = dict.erase(it);
		else
			++it;
	}

	decltype(dict) dict2;

	auto v1 = Vector2D { 1.0, 3.0 };
	auto v2 = Vector2D { 2.9, 33.2 };

	auto result = v1 * v2;

	cout << "result: " << result << endl;

	system("PAUSE");
}