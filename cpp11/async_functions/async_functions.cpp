#include <iostream>
#include <future>
#include <thread>

using namespace std;

int factorial(int n)
{
	this_thread::sleep_for(chrono::milliseconds(rand()%1000));

	if (n == 1)
		return 1;
	else return factorial(n - 1) * n;
}

int main()
{
	vector <future<int>> factorials;

	for (int i = 1; i < 10; ++i)
		factorials.push_back(async(launch::async, [i] { return factorial(i); })); 

	//for (int i = 0; i < 50; ++i)
	//{
	//	cout << ".";
	//	this_thread::sleep_for(chrono::milliseconds(100));
	//}

	for (auto& f : factorials)
	{
		cout << f.get() << endl;
	}

	system("PAUSE");
}