#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <random>
#include <numeric>
#include <atomic>
#include <mutex>
#include <condition_variable>

using namespace std;

class Data
{
	vector<int> data_;
	atomic<bool> is_ready {false};
public:
	void read()
	{
		cout << "Start reading..." << endl;

		this_thread::sleep_for(chrono::seconds(2));

		data_.resize(100);

		random_device rd;
		mt19937_64 gen(rd());
		uniform_int_distribution<> dist(0, 1000);

		generate(data_.begin(), data_.end(), [&]{ return dist(gen); });

		is_ready = true;

		cout << "read has ended..." << endl;
	}

	void process(int id)
	{
		cout << "process started..." << endl;

		while (!is_ready);

		long sum = accumulate(data_.begin(), data_.end(), 0L);

		cout << "Id:" << id << "; Sum: " << sum << endl;
	}
};

namespace alt
{
	class Data
	{
		vector<int> data_;
		bool is_ready_{ false };
		mutex ready_mtx_;
		condition_variable cv_ready_;
	public:
		void read()
		{
			cout << "Start reading..." << endl;

			this_thread::sleep_for(chrono::seconds(2));

			data_.resize(100);

			random_device rd;
			mt19937_64 gen(rd());
			uniform_int_distribution<> dist(0, 1000);

			generate(data_.begin(), data_.end(), [&]{ return dist(gen); });

			unique_lock<mutex> lk(ready_mtx_);
			is_ready_ = true;
			lk.unlock();
			cv_ready_.notify_all();

			cout << "read has ended..." << endl;
		}

		void process(int id)
		{
			cout << "process started..." << endl;

			unique_lock<mutex> lk(ready_mtx_);
			cv_ready_.wait(lk, [this] { return is_ready_; });

			long sum = accumulate(data_.begin(), data_.end(), 0L);

			cout << "Id:" << id << "; Sum: " << sum << endl;
		}
	};
}

void use_data()
{
	alt::Data data;

	auto thd1 = thread{ [&data] { data.read(); } };
	auto thd2 = thread{ [&data] { data.process(1); } };
	auto thd3 = thread{ [&data] { data.process(2); } };


	thd1.join();
	thd2.join();
	thd3.join();
}

int main()
{
	use_data();

	system("PAUSE");
}