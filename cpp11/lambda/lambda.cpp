#include <iostream>
#include <string>
#include <memory>
#include <map>
#include <functional>
#include <queue>
#include <set>

using namespace std;

class MagicType
{
public:
	void operator()() const
	{
		cout << "Hello from lambda" << endl;
	}
};

void print()
{
	cout << "Print" << endl;
}

struct Printer
{
	void operator()()
	{
		cout << "Printer" << endl;
	}
};

using Task = function<void()>;

class Worker
{
	queue<Task> tasks_;
public:
	void register_task(Task&& t)
	{
		tasks_.push(move(t));
	}

	void run()
	{
		while (!tasks_.empty())
		{
			Task t = tasks_.front();
			t(); // call
			tasks_.pop();
		}
	}
};

void function_explained()
{
	function<void()> f1 = &print;
	f1();

	f1 = Printer();
	f1();

	f1 = []{ cout << "Lambda printer..."; };
	f1();
}

class Db
{
public:
	void save(string msg)
	{
		cout << "INSERT INTO Logs VALUES(" << msg << ")" << endl;
	}

	Db() = default;

	Db(const Db&) = delete;
	Db& operator=(const Db&) = delete;

	~Db()
	{
		cout << "After birds... db" << endl;
	}
};

struct MagicType2
{
	const shared_ptr<Db> database_;
public:
	// ctor
	MagicType2(shared_ptr<Db> db) : database_(db)
	{}

	void operator()() const
	{
		database_->save("End of work");
	}
};

int main()
{
	function_explained();

	[](){}(); // pure C++11
	auto l = [] { cout << "Hello from lambda" << endl; };
	
	l();
	l();

	auto add = [](int a, int b) { return a + b; };

	cout << "5 + 6 = " << add(5, 6) << endl;

	int(*fun)(int, int) = [](int a, int b) { return a + b; };

	cout << "5 + 6 = " << fun(5, 6) << endl;

	map<string, int(*)(int, int)> operations;

	operations.insert(make_pair("*", [](int a, int b) { return a * b; }));

	cout << operations["*"](2, 4) << endl;

	Worker worker;
	{
		shared_ptr<Db> database = make_shared<Db>(); 

		worker.register_task(&print);
		worker.register_task(Printer());
		worker.register_task([database]{ database->save("End of work"); });

		cout << "run..." << endl;
	}
	worker.run();

	auto comp_by_value = [](const unique_ptr<int>& a, const unique_ptr<int>& b) { return *a < *b; };

	//set<unique_ptr<int>, decltype(comp_by_value)> set1(comp_by_value); 
	using Comparer_by_value = function<bool(const unique_ptr<int>& a, const unique_ptr<int>& b)> ;
	set<unique_ptr<int>, Comparer_by_value> set1(comp_by_value);

	set1.emplace(new int(23));
	set1.insert(unique_ptr<int>(new int(12)));
	set1.emplace(new int(55));

	for (const auto& item : set1)
		cout << *item << " ";
	cout << endl;

	system("PAUSE");
}