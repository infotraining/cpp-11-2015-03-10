#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <thread>
#include <atomic>

using namespace std;

int main()
{
    long N = 100'000'000;
    long counter = 0;

    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    cout << "Pi calc!" << endl;
    auto start = chrono::high_resolution_clock::now();

    for (int n = 0 ; n < N ; ++n)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            counter++;
    }
    auto end = chrono::high_resolution_clock::now();
    cout << "Pi = " << double(counter)/double(N)*4 << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;

    return 0;
}

