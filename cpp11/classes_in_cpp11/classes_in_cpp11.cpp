#include <iostream>
#include <string>
#include <vector>

using namespace std;

class NoCopyableArray
{
	int* data_;
	size_t size_;
public:
	NoCopyableArray(size_t size) : data_(new int[size]), size_(size)
	{
		fill(data_, data_ + size_, 0);

		cout << "Ctor NoCopyableArray with " << data_ << endl;
	}

	NoCopyableArray(const NoCopyableArray&) = delete;
	NoCopyableArray& operator=(const NoCopyableArray&) = delete;

	~NoCopyableArray()
	{
		delete[] data_;
	}

	int& operator[](size_t index)
	{
		return data_[index];
	}

	const int& operator[](size_t index) const
	{
		return data_[index];
	}

	size_t size() const
	{
		return size_;
	}
};

void foo(int x)
{
	cout << "foo(int: " << x << ")" << endl;
}

void foo(double) = delete;

int get_id()
{
	return 42;
}

class Gadget
{
	int id_ = get_id();
	string name_ = "unknown";

public:
	Gadget() = default;

	Gadget(const string& name) : name_(name) 
	{}

	virtual ~Gadget() = default;

	int id() const
	{
		return id_;
	}

	virtual void play() const
	{
		cout << "Gadget::play()" << endl;
	}

	string name() const
	{
		return name_;
	}
};

class SuperGadget final : public Gadget 
{
public:
	//SuperGadget() = default;

	//SuperGadget(const string& name) : Gadget(name)
	//{}

	using Gadget::Gadget;

	void play() const override
	{
		cout << "SuperGadget::play()" << endl;
	}
};

//class ExtraGadget : public SuperGadget
//{
//
//};

int main()
{
	NoCopyableArray arr1{ 10 };

	arr1[0] = 13;

	//NoCopyableArray arr2 = arr1;

	foo(23);
	
	//foo(234.55);

	//float fx = 34.24F;

	//foo(fx);

	Gadget g1;

	cout << g1.name() << endl;

	Gadget g2{ "mp3 player" };

	cout << g2.id() << " " << g2.name() << endl;

	string str = "4stopy";

	int value = stoi(str);

	cout << value << endl;

	/*Gadget* ptr_gadget = new SuperGadget("gadget");

	ptr_gadget->play();

	delete ptr_gadget;*/

	system("PAUSE");
}