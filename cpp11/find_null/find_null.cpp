#include <iostream>
#include <vector>
#include <iterator>
#include <memory>
#include <cassert>
#include <algorithm>
#include <functional>

using namespace std;

template <typename Container>
auto find_null(Container& container) -> decltype(begin(container))
{
	auto it = begin(container);

	while (it != end(container))
	{
		if (*it == nullptr)
			return it;

		++it;
	}

	return it;
}

int main()
{
	// find_null: Given a container of pointers, return an
	// iterator to the first null pointer (or the end
	// iterator if none is found)

	int x{ 10 };
	int y{ 20 };

	int* tab1[] = { &x, nullptr, &y };

	int** where_null1 = find_null(tab1);
	assert((where_null1 - tab1) == 1);
	
	const vector<int*> vec_ptr(begin(tab1), end(tab1));

	auto where_null2 = find_null(vec_ptr);

	assert(distance(vec_ptr.begin(), where_null2) == 1);

	auto ptr_is_null = [](int* ptr) { return ptr == nullptr; };

	auto where_is_null = find_if(vec_ptr.begin(), vec_ptr.end(), ptr_is_null);

	system("PAUSE");
}