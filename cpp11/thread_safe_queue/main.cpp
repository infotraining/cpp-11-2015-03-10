#include <iostream>
#include <queue>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <algorithm>
#include <string>

#include "thread_safe_queue.hpp"

using namespace std;

typedef string DataType;

class ProducerConsumer
{
    ThreadSafeQueue<DataType> queue_;
    static const string end_of_work;

public:
    void produce(const DataType& data, int consumer_count)
    {
        DataType item = data;

        while(next_permutation(item.begin(), item.end()))
        {
            cout << "Producing: " << item << endl;
            queue_.push(item);
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
        }

        // end of job
        for(int i = 0; i <  consumer_count; ++i)
            queue_.push(end_of_work);
    }

    void consumer(int id)
    {
        while (true)
        {
            DataType item;
            queue_.wait_and_pop(item);

            if (item != end_of_work)
            {
                cout << "Consumer#" << id << " processed an item: "
                     << process_data(item) << endl;
            }
            else
            {
                cout << "End of job - consumer#" << id << endl;
                return;
            }
        }
    }

    DataType process_data(const DataType& data)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(300));

		string upper_case_data(data);
		transform(data.begin(), data.end(), upper_case_data.begin(), toupper);

        return upper_case_data;
    }
};

const string  ProducerConsumer::end_of_work = "";

int main()
{
    ProducerConsumer pc;

    std::thread thd1( [&pc] { pc.produce("abcd", 2); });
    std::thread thd2( [&pc] { pc.consumer(1); });
    std::thread thd3([&pc] { pc.consumer(2); });

    thd1.join();
    thd2.join();
    thd3.join();
}
