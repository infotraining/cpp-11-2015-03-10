#include <iostream>
#include <vector>
#include <string>
#include <map>

using namespace std;

unsigned int get_value()
{
	return 31457;
}

struct Aggregate
{
	int x;
	double dx;

	Aggregate(double dx) : x{ static_cast<int>(dx) }, dx{ dx }
	{}
};

Aggregate agg{ 5.4 };

struct X
{
	X()
	{
		cout << "X() ctor" << endl;
	}

	explicit X(int value) : value_(value)
	{
		cout << "X(" << value_ << ") ctor" << endl;
	}

	int value_ { 1 };
};

X x1{};
X x2{ static_cast<int>(get_value()) };
X x3(34);  // X x3 = X(34)

class Container
{
	vector<int> data_;
public:
	Container()
	{
		cout << "Container default ctor" << endl;
	}

	Container(initializer_list<int> items) : data_(items)
	{
		cout << "Container ctor with il" << endl;
	}

	Container(int size)
	{
		data_.resize(size);
	}

	void print() const
	{
		cout << "Items: ";
		for (const auto& item : data_)
			cout << item << " ";
		cout << endl;
	}
};

int main()
{
	int rx(get_value());

	cout << "rx: " << rx << endl;

	//initializer_list<string> lst = { "jan", "adam", "zenon" };
	//vector<string> words{ lst };

	vector<string> words = { "jan", "adam", "zenon" };

	for (const auto& w : words)
		cout << w << endl;

	map<int, string> dict = { { 1, "one" }, { 2, "two" }, { 3, "three" } };

	cout << dict[2] << endl;

	Container cont1{ 1, 2, 3, 4, 6 };

	cont1.print();

	Container cont2{ 10 };

	cont2.print();

	Container cont3;

	cont3.print();

	vector<int> vec1(5, 10);
	vector<int> vec2{ 5, 10 };

	vector<double> vec3{ 10, 3.14 };

	vector<int> vec4({ 1, 2, 3 });
}
